package com.example.excel.model;

import jakarta.persistence.*;

@Entity
@Table(name = "page")
public class Page {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "id_role")
    private int idrole;

    @Column(name = "page_name")
    private String pageName;

    @Column(name = "allowed_access")
    private Boolean allowedAccess;

    public Page() {
    }

    public Page(int id, int idrole, String pageName, Boolean allowedAccess) {
        this.id = id;
        this.idrole = idrole;
        this.pageName = pageName;
        this.allowedAccess = allowedAccess;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdrole() {
        return idrole;
    }

    public void setIdrole(int idrole) {
        this.idrole = idrole;
    }

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    public Boolean getAllowedAccess() {
        return allowedAccess;
    }

    public void setAllowedAccess(Boolean allowedAccess) {
        this.allowedAccess = allowedAccess;
    }
}

