package com.example.excel.model;


public class AuthRequest {

    private String username;
    private String password;
    private String idroles;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getIdroles() {
        return idroles;
    }

    public void setIdroles(String idroles) {
        this.idroles = idroles;
    }
}

