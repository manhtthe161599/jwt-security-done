package com.example.excel.config;

import com.example.excel.model.Roles;
import com.example.excel.model.Users;
import com.example.excel.service.RolesRepository;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class UserInfoDetails implements UserDetails {

    private String name;
    private String password;
    private List<GrantedAuthority> authorities;

    public UserInfoDetails(Users userInfo, RolesRepository rolesRepository) {
        name = userInfo.getName();
        password = userInfo.getPassword();

        Roles role = rolesRepository.findById(userInfo.getIdroles()).orElse(null);

        if (role != null) {
            authorities = List.of(new SimpleGrantedAuthority(role.getRoleName()));
        } else {
            authorities = List.of();
        }
    }
    public String getRoleName() {
        return getRoleName();
    }

    public UserInfoDetails(Users userInfo) {
        this.name = userInfo.getName();
        this.password = userInfo.getPassword();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return name;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}



