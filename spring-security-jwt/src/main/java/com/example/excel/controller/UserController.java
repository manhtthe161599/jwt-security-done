package com.example.excel.controller;


import com.example.excel.config.JwtService;
import com.example.excel.config.UserInfoDetails;
import com.example.excel.config.UserInfoService;
import com.example.excel.model.AuthRequest;
import com.example.excel.model.Page;
import com.example.excel.model.Users;
import com.example.excel.news.PageService;
import com.example.excel.service.UserRepository;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;


import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;


@RestController
@RequestMapping("/auth")
public class UserController {

    @Value("${auth.api.baseurl}")
    private String authApiBaseUrl;

    @Value("${jwt.secret}")
    private String SECRET;


    @Autowired
    private JwtService jwtService;
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private PageService pageService;

    @Autowired
    private UserRepository userRepository;



    // ****** trang ADMIN
    @GetMapping("/admin/adminProfile")
    public ResponseEntity<String> adminProfile(@RequestHeader("Authorization") String jwtToken) {
        boolean canAccessAdminPage = pageService.hasAccess("admin", jwtToken.substring(7));
        if (canAccessAdminPage) {
            return ResponseEntity.ok("Hello Admin !!");
        } else {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("không cho vào !!!!");
        }
    }

    // ****** trang USER
    @GetMapping("/user/userProfile")
    public ResponseEntity<String> userProfile(@RequestHeader("Authorization") String jwtToken) {
        boolean canAccessAdminPage = pageService.hasAccess("user", jwtToken.substring(7));
        if (canAccessAdminPage) {
            return ResponseEntity.ok("Hello User !!");
        } else {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("không cho vào !!!!!");
        }
    }

    //*** trả về jwt
    @PostMapping("/generateToken")
    public ResponseEntity<?> authenticateAndGetToken(@RequestBody AuthRequest authRequest) {
        try {
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(authRequest.getUsername(), authRequest.getPassword())
            );

            if (authentication.isAuthenticated()) {
                UserDetails userDetails = (UserDetails) authentication.getPrincipal();
                int userId = userRepository.findByName(userDetails.getUsername()).getId();
                final String token = jwtService.generateToken(userDetails.getUsername(), userId);

                return ResponseEntity.ok(token);
            } else {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Authentication failed");
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal server error");
        }
    }


    // ****** set quyền truy cập cho roles
    @PostMapping("/setUserAccess")
    public ResponseEntity<String> setUserAccess(@RequestBody Page page) {
        try {
            if (page.getIdrole() <= 0 || page.getPageName() == null || page.getPageName().isEmpty()) {
                return ResponseEntity.badRequest().body("Invalid input data");
            }
            Page existingPage = pageService.getPageByUserIdAndPageName(page.getIdrole(), page.getPageName());

            if (existingPage == null) {
                existingPage = new Page();
                existingPage.setIdrole(page.getIdrole());
                existingPage.setPageName(page.getPageName());
            }
            existingPage.setAllowedAccess(page.getAllowedAccess());

            // Lưu hoặc cập nhật thông tin vào cơ sở dữ liệu
            pageService.savePage(existingPage);

            return ResponseEntity.ok("thành công !!!");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error while setting user access: " + e.getMessage());
        }
    }


}

