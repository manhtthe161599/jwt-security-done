package com.example.excel.news;

import com.example.excel.config.JwtService;
import com.example.excel.model.Page;
import com.example.excel.model.Roles;
import com.example.excel.service.PageRepository;
import com.example.excel.service.RolesRepository;
import com.example.excel.service.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PageService {

    @Autowired
    private PageRepository pageRepository;
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JwtService jwtService;



    public Page getPageByUserIdAndPageName(int idrole, String pageName) {
        return pageRepository.findByRoleAndPageName(idrole, pageName);
    }

    public void savePage(Page page) {
        pageRepository.save(page);
    }


    public boolean hasAccess(String pageName, String jwtToken) {
        int userId = jwtService.extractUserId(jwtToken);
        if (userId <= 0) {
            return false;
        }

        int userRoleId = userRepository.findUserRolesById(userId);
        if (userRoleId <= 0) {
            return false;
        }

        Page page = pageRepository.findByRoleAndPageNameWithAccess(userRoleId, pageName);
        return page != null && page.getAllowedAccess();
    }


}


