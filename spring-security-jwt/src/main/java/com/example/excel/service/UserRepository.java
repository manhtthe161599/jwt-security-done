package com.example.excel.service;

import com.example.excel.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<Users, Integer> {

    @Query("SELECT u.idroles FROM Users u WHERE u.id = :userId")
    int findUserRolesById(@Param("userId") int userId);

    Users findByName(String name);

}





