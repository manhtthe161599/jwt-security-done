package com.example.excel.service;

import com.example.excel.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserInfoRepository extends JpaRepository<Users, Integer> {
    Optional<Users> findByName(String username);
    Optional<Users> findByEmail(String email);
}

