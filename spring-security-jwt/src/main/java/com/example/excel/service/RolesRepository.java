package com.example.excel.service;

import com.example.excel.model.Roles;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface RolesRepository extends JpaRepository<Roles, Integer> {
    @Query("SELECT r.roleName FROM Roles r JOIN Users u ON r.id = u.idroles WHERE u.id = :userId")
    String findRoleNameByUserId(@Param("userId") int userId);

    Roles findByRoleName(String roleName);
}

