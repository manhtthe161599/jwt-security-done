package com.example.excel.service;


import com.example.excel.model.Page;
import com.example.excel.model.Roles;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PageRepository extends JpaRepository<Page, Integer> {

    @Query("SELECT p FROM Page p WHERE p.idrole = :idroles AND p.pageName = :pageName")
    Page findByRoleAndPageName(@Param("idroles") int idroles, @Param("pageName") String pageName);

    @Query("SELECT p FROM Page p WHERE p.idrole = :idroles AND p.pageName = :pageName")
    Page findByRoleAndPageNameWithAccess(@Param("idroles") int idroles, @Param("pageName") String pageName);
}




